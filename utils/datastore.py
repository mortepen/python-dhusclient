import json

from . import commons
from cliff.show import ShowOne


class DatastoreType(object):
    def __init__(self, label, odata_type):
        self.label = label
        self.odata_type = odata_type
        self.properties = ['Name', 'Type', 'Priority', 'Read Only', 'Auto Evict', 'Size', 'Capacity']

    def get_iterable_data(self, json_data):
        return [
            json_data['Name'],
            self.label,
            json_data['Priority'],
            json_data['ReadOnly'],
            json_data['AutoEviction'],
            json_data['CurrentSize'],
            json_data['MaximumSize']
        ]

    def get_label(self):
        return self.label

    def get_type(self):
        return self.odata_type

    def get_properties(self):
        return self.properties


class HfsDatastoreType(DatastoreType):
    def __init__(self):
        super(HfsDatastoreType, self).__init__('hfs', '#OData.DHuS.HFSDataStore')
        self.properties = super(HfsDatastoreType, self).get_properties()
        self.properties.append('Path')
        self.properties.append('File Depth')
        self.properties.append('Max Item')

    def get_iterable_data(self, json_data):
        data = super(HfsDatastoreType, self).get_iterable_data(json_data)
        data.append(json_data['Path'])
        data.append(json_data['MaxFileDepth'])
        data.append(json_data['MaxItems'])
        return data


class SwiftDatastoreType(DatastoreType):
    def __init__(self):
        super(SwiftDatastoreType, self).__init__('swift', '#OData.DHuS.OpenStackDataStore')
        self.properties = super(SwiftDatastoreType, self).get_properties()
        self.properties.append('Url')
        self.properties.append('Identity')
        self.properties.append('Region')
        self.properties.append('Container')


DATASTORE_TYPES = [HfsDatastoreType(), SwiftDatastoreType()]


class CreateHfs(ShowOne):
    NAME = 'datastore create hfs'
    DESCRIPTION = 'Create a HFS datastore'

    def __init__(self, app, app_args):
        super(CreateHfs, self).__init__(app, app_args, self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def get_parser(self, prog_name):
        parser = super(CreateHfs, self).get_parser(prog_name)
        add_default_create_option(parser)
        parser.add_argument('--path', type=str, help='datastore path', required=True)
        parser.add_argument('--file-depth', type=int, default=10, help='(default 10)')
        parser.add_argument('--max-items', type=int, default=1024, help='(default 1024)')
        return parser

    def take_action(self, parsed_args):
        data_type = get_datastore_type_by_label('hfs')
        data = hfs_creation_dict(parsed_args)
        response = self.app.client.create('v2/DataStores', data)
        data = json.JSONDecoder().decode(response)
        return data_type.get_properties(), data_type.get_iterable_data(data)


class ReadDataStore(ShowOne):
    NAME = 'datastore show'
    DESCRIPTION = 'Show in detail a datastore'

    def __init__(self, app, app_args):
        super(ReadDataStore, self).__init__(app, app_args, self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def get_parser(self, prog_name):
        parser = super(ReadDataStore, self).get_parser(prog_name)
        parser.add_argument('name', type=str, help='datastore name')
        return parser

    def take_action(self, parsed_args):
        response = self.app.client.get("v2/DataStores('" + parsed_args.name + "')")
        data = json.JSONDecoder().decode(response)
        data_type = get_datastore_type_by_type(data['@odata.type'])
        return data_type.get_properties(), data_type.get_iterable_data(data)


def get_datastore_type_by_type(datastore_type):
    for ds_type in DATASTORE_TYPES:
        if ds_type.get_type() == datastore_type:
            return ds_type
    return DatastoreType('unknown', 'unknown')


def get_datastore_type_by_label(label):
    for ds_type in DATASTORE_TYPES:
        if ds_type.get_label() == label:
            return ds_type
    return DatastoreType('unknown', 'unknown')


def add_default_create_option(parser):
    parser.add_argument('--priority', type=int, help='datastore priority')
    parser.add_argument('--max-size', type=int, help='set maximum datastore size')
    parser.add_argument('--read-only', action='store_true', help='set the datastore read only')
    parser.add_argument('--auto-evict', action='store_true', help='active auto eviction')
    parser.add_argument('name', type=str, help='datastore name')


def default_write_dict(parsed_arg):
    data = {
        'Name': parsed_arg.name,
        'ReadOnly': parsed_arg.read_only,
        'AutoEviction': parsed_arg.auto_evict
    }
    if parsed_arg.priority is not None:
        data['Priority'] = parsed_arg.priority
    if parsed_arg.max_size is not None:
        data['MaxSize'] = parsed_arg.max_size
    return data


def hfs_creation_dict(parsed_arg):
    data = default_write_dict(parsed_arg)
    data['@odata.type'] = get_datastore_type_by_label('hfs').get_type()
    data['Path'] = parsed_arg.path
    data['MaxFileDepth'] = parsed_arg.file_depth
    data['MaxItems'] = parsed_arg.max_items
    return data


def list_commands():
    return [
        commons.DhusCommand(CreateHfs.NAME, CreateHfs.DESCRIPTION, CreateHfs),
        commons.DhusCommand(ReadDataStore.NAME, ReadDataStore.DESCRIPTION, ReadDataStore)
    ]
