import argparse
import json

from cliff.command import Command
from cliff.show import ShowOne
from cliff.lister import Lister
from . import commons


class CollectionAdd(Command):
    """Allows to link collections to an eviction"""
    NAME = 'eviction collection add'
    DESCRIPTION = 'Link collections to an eviction'

    def __init__(self, app, app_arg):
        super(CollectionAdd, self).__init__(app, app_arg, self.NAME)

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(prog=prog_name, description=self.DESCRIPTION)
        parser.add_argument('eviction', type=str, help='eviction name')
        parser.add_argument('collection', type=str, nargs='+', help='collection name')
        return parser

    def take_action(self, parsed_args):
        raise NotImplementedError('not supported operation')


class CollectionList(Command):
    """Allows to list linked collections to an eviction"""
    NAME = 'eviction collection list'
    DESCRIPTION = 'List linked collections to an eviction'

    def __init__(self, app, app_arg):
        super(CollectionList, self).__init__(app, app_arg, self.NAME)

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(prog=prog_name, description=self.DESCRIPTION)
        return parser

    def take_action(self, parsed_args):
        raise NotImplementedError('not supported operation')


class CollectionRemove(Command):
    """Allows to unlink collections to an eviction"""
    NAME = 'eviction collection remove'
    DESCRIPTION = 'Unlink collections to an eviction'

    def __init__(self, app, app_arg):
        super(CollectionRemove, self).__init__(app, app_arg, self.NAME)

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(prog=prog_name, description=self.DESCRIPTION)
        parser.add_argument('eviction', type=str, help='eviction name')
        parser.add_argument('collection', type=str, nargs='+', help='collection name')
        return parser

    def take_action(self, parsed_args):
        raise NotImplementedError('not supported operation')


class Create(ShowOne):
    """Allows to create an eviction"""
    NAME = 'eviction create'
    DESCRIPTION = 'Create an eviction'

    def __init__(self, app, app_arg):
        super(Create, self).__init__(app, app_arg, self.NAME)
        self.parser = self.get_parser(self.NAME)

    def get_parser(self, prog_name):
        parser = super(Create, self).get_parser(prog_name)
        parser = option_parser_create_and_set(parser)
        parser.add_argument('name', help='eviction name')
        return parser

    def take_action(self, parsed_args):
        data = args_to_json(parsed_args)
        response = self.app.client.create('v2/Evictions', data)
        json_data = json.JSONDecoder().decode(response)
        return COLUMNS_READ, json_to_read_tuple(json_data)


class Read(ShowOne):
    """Allows to show in detail an eviction"""
    NAME = 'eviction show'
    DESCRIPTION = 'Show'

    def __init__(self, app, app_arg):
        super(Read, self).__init__(app, app_arg, self.NAME)
        self.parser = self.get_parser(self.NAME)

    def get_parser(self, prog_name):
        parser = super(Read, self).get_parser(prog_name)
        parser.add_argument('name', metavar='<eviction>', type=str, help='eviction name')
        return parser

    def take_action(self, parsed_args):
        response = self.app.client.get("v2/Evictions('" + parsed_args.name + "')")
        data = json.JSONDecoder().decode(response)
        return COLUMNS_READ, json_to_read_tuple(data)


class List(Lister):
    """Allows to list evictions"""
    NAME = 'eviction list'
    DESCRIPTION = 'List evictions'

    def __init__(self, app, app_arg):
        super(List, self).__init__(app, app_arg, self.NAME)
        self.parser = self.get_parser(self.NAME)

    def get_parser(self, prog_name):
        parser = super(List, self).get_parser(prog_name)
        return parser

    def need_sort_by_cliff(self):
        return False

    def take_action(self, parsed_args):
        values = []
        data = json.JSONDecoder().decode(self.app.client.get('v2/Evictions'))
        for entry in data['value']:
            values.append(json_to_list_tuple(entry))
        return COLUMNS_LIST, values


class Update(Command):
    """Allows to update an eviction"""
    NAME = 'eviction set'
    DESCRIPTION = 'Update an eviction'

    def __init__(self, app, app_args):
        super(Update, self).__init__(app, app_args, self.NAME)

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(prog=prog_name, description=self.DESCRIPTION)
        parser = option_parser_create_and_set(parser)
        parser.add_argument(
            '--no-cron',
            action='store_true',
            help='remove cron property'
        )
        parser.add_argument(
            'name',
            type=str,
            help='eviction name to update'
        )
        return parser

    def take_action(self, parsed_args):
        data = args_to_json(parsed_args)
        name = data['Name']
        del data['Name']
        if self.app.client.update("v2/Evictions('" + name + "')", data):
            return 0
        return 1


class Delete(Command):
    """Allow to delete evictions"""
    NAME = 'eviction delete'
    DESCRIPTION = 'Delete evictions'

    def __init__(self, app, app_args):
        super(Delete, self).__init__(app, app_args, self.NAME)

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(prog=prog_name, description=self.DESCRIPTION)
        parser.add_argument(
            'name',
            type=str,
            nargs='+',
            default=[],
            help='eviction name to delete'
        )
        return parser

    def take_action(self, parsed_args):
        return_value = 0
        for name in parsed_args.name:
            if not self.app.client.delete("v2/Evictions('" + name + "')"):
                self.app.stderr.write('Cannot delete eviction: ' + name + '\n')
                return_value = 1
        return return_value


COLUMNS_READ = ['Name', 'Keep', 'Max', 'Soft', 'Status', 'Cron', 'Filter', 'Order']
COLUMNS_LIST = ['Name', 'Keep', 'Max', 'Soft', 'Status']


def args_to_json(parsed_args):
    data = {}
    try:
        data['Name'] = parsed_args.name
    except AttributeError:
        pass
    try:
        if parsed_args.keep_period is not None:
            data['KeepPeriod'] = parsed_args.keep_period
    except AttributeError:
        pass
    try:
        if parsed_args.keep_unit is not None:
            data['KeepPeriodUnit'] = parsed_args.keep_unit
    except AttributeError:
        pass
    try:
        if parsed_args.max_evict is not None:
            data['MaxEvictedProducts'] = parsed_args.max_evict
    except AttributeError:
        pass
    try:
        if parsed_args.soft_evict is not None:
            data['SoftEviction'] = parsed_args.soft_evict
    except AttributeError:
        pass
    try:
        if parsed_args.filter is not None:
            data['Filter'] = parsed_args.filter
    except AttributeError:
        pass
    try:
        if parsed_args.order is not None:
            data['OrderBy'] = parsed_args.order
    except AttributeError:
        pass

    cron_remove = None
    cron_activation = None
    cron_schedule = None
    try:
        if parsed_args.no_cron:
            cron_remove = True
    except AttributeError:
        pass
    try:
        cron_activation = parsed_args.cron_activation
    except AttributeError:
        pass
    try:
        cron_schedule = parsed_args.cron_schedule
    except AttributeError:
        pass
    if cron_remove is not None and cron_remove:
        data['Cron'] = None
    else:
        data['Cron'] = {}
        if cron_activation is not None:
            data['Cron']['Active'] = cron_activation
        if cron_schedule is not None:
            data['Cron']['Schedule'] = cron_schedule
        if len(data['Cron']) == 0:
            del data['Cron']

    return data


def json_to_read_tuple(json_data):
    return (
        json_data['Name'],
        str(json_data['KeepPeriod']) + ' ' + json_data['KeepPeriodUnit'],
        json_data['MaxEvictedProducts'],
        json_data['SoftEviction'],
        json_data['Status'],
        commons.cron_formatter(json_data['Cron']),
        json_data['Filter'],
        json_data['OrderBy']
    )


def json_to_list_tuple(json_data):
    return (
        json_data['Name'],
        str(json_data['KeepPeriod']) + ' ' + json_data['KeepPeriodUnit'],
        json_data['MaxEvictedProducts'],
        json_data['SoftEviction'],
        json_data['Status'],
    )


def option_parser_create_and_set(parser):
    parser.add_argument(
        '-period', '--keep-period',
        type=int,
        help='minimal keep period for a product')
    parser.add_argument(
        '-unit', '--keep-unit',
        type=str,
        choices=['DAYS', 'HOURS', 'MINUTES'],
        help='keep period unit (default DAYS)'
    )
    parser.add_argument(
        '-max', '--max-evict',
        type=int,
        help='maximum product to evict per activation'
    )
    parser.add_argument(
        '-hard', '--hard-evict',
        action='store_const',
        const=False,
        dest='soft_evict',
        help='delete product completely'
    )
    parser.add_argument(
        '-soft', '--soft-evict',
        action='store_const',
        const=True,
        dest='soft_evict',
        help='delete product data but its reference'
    )
    parser.add_argument(
        '--cron-enable',
        action='store_const',
        const=True,
        dest='cron_activation',
        help='enabling cron activation. Useless if --no-cron present'
    )
    parser.add_argument(
        '--cron-disable',
        action='store_const',
        const=False,
        dest='cron_activation',
        help='enabling cron activation. Useless if --no-cron present'
    )
    parser.add_argument(
        '--cron-schedule',
        type=str,
        default=None,
        help='cron quartz expression. Useless if --no-cron present'
    )
    parser.add_argument(
        '--filter',
        type=str,
        help='product OData filter expression for custom eviction'
    )
    parser.add_argument(
        '--order',
        type=str,
        help='product OData order expression for custom eviction'
    )
    return parser


def list_commands():
    return [
       commons.DhusCommand(CollectionAdd.NAME, CollectionAdd.DESCRIPTION, CollectionAdd),
       commons.DhusCommand(CollectionList.NAME, CollectionList.DESCRIPTION, CollectionList),
       commons.DhusCommand(CollectionRemove.NAME, CollectionRemove.DESCRIPTION, CollectionRemove),
       commons.DhusCommand(Create.NAME, Create.DESCRIPTION, Create),
       commons.DhusCommand(Read.NAME, Read.DESCRIPTION, Read),
       commons.DhusCommand(List.NAME, List.DESCRIPTION, List),
       commons.DhusCommand(Update.NAME, Update.DESCRIPTION, Update),
       commons.DhusCommand(Delete.NAME, Delete.DESCRIPTION, Delete)
    ]
