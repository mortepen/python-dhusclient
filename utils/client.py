import requests

from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError
from .exception import DhusClientException


class DhusClient(object):
    def __init__(self, url, username=None, password=''):
        self._odata_url = url + '/odata/'
        if username is None:
            self._auth = None
        else:
            self._auth = HTTPBasicAuth(username, password)

    def create(self, path, data):
        url = self._odata_url + path
        headers = {'Content-Type': 'application/json'}
        try:
            response = requests.post(url, headers=headers, auth=self._auth, json=data)
        except ConnectionError:
            raise DhusClientException('Cannot reach server')
        check_status_code(response, 201)
        return response.text

    def get(self, path, skip=None, top=None, filter=None, order=None):
        url = self._odata_url + path
        params = generate_parameters(skip, top, filter, order)
        try:
            response = requests.get(url, params=params, auth=self._auth)
        except ConnectionError:
            raise DhusClientException('Cannot reach server')
        check_status_code(response, 200, 202, 206)
        return response.text

    def update(self, path, data):
        url = self._odata_url + path
        params = {'Content-Type': 'application/json'}
        try:
            if path.startswith('v1'):
                response = requests.put(url, params=params, auth=self._auth, json=data)
            else:
                response = requests.request('PATCH', url, params=params, auth=self._auth, json=data)
            check_status_code(response, 200, 204)
        except ConnectionError:
            raise DhusClientException('Cannot reach server')
        except DhusClientException:
            return False
        return True

    def delete(self, path):
        url = self._odata_url + path
        try:
            response = requests.delete(url, auth=self._auth)
            check_status_code(response, 200, 204)
        except ConnectionError:
            raise DhusClientException('Cannot reach server')
        except DhusClientException:
            return False
        return True

    def perform_action(self, path, action_name, param={}):
        raise NotImplementedError

    def perform_function(self, path, function_name, param={}):
        raise NotImplementedError


def generate_parameters(skip, top, filter, order):
    parameters = {'$format': 'json'}
    if filter is not None:
        parameters['$filter'] = filter
    if order is not None:
        parameters['$orderby'] = order
    if skip is not None:
        parameters['$skip'] = skip
    if top is not None:
        parameters['$top'] = top
    return parameters


def check_status_code(http_response, *codes):
    if http_response.status_code not in codes:
        raise_error(http_response)


def raise_error(http_response):
    if http_response.status_code == 400:
        raise DhusClientException('internal client error')
    if http_response.status_code == 401:
        raise DhusClientException('invalid login/password')
    if http_response.status_code == 403:
        raise DhusClientException('permission denied')
    if http_response.status_code == 404:
        raise DhusClientException('not found')
    if http_response.status_code == 416:
        raise DhusClientException('invalid requested range')
    if http_response.status_code == 500:
        raise DhusClientException('DHuS server error')
    if http_response.status_code == 503:
        raise DhusClientException('Service is unavailable or in maintenance state')
    raise DhusClientException('Unexpected status code: ' + http_response.status_code)
