class PropertyColumnAdapter(object):
    """Allows to retrieve associated column name of a property"""
    def __init__(self, columns, properties):
        self.columns = columns
        self.properties = properties

    def get_column(self, prop):
        column_index = self.properties.index(prop)
        if column_index == -1:
            return None
        return self.columns[column_index]

    def get_property(self, column):
        property_index = self.columns.index(column)
        if property_index == -1:
            return None
        return self.properties[property_index]


class DhusCommand(object):
    def __init__(self, name, description, clazz):
        self.name = name
        self.description = description
        self.clazz = clazz

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_class(self):
        return self.clazz


def cron_formatter(cron):
    """Returns str representation of an OData Cron DHuS"""
    text = ''
    if cron is not None and type(cron) is dict:
        try:
            active = cron['Active']
            schedule = cron['Schedule']
            text = schedule + '('
            if active:
                text = text + 'enabled'
            else:
                text = text + 'disabled'
            text = text + ')'
        except ValueError:
            pass

    if text == '':
        return None
    return text
