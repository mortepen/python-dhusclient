import argparse
import json

from cliff.command import Command
from cliff.show import ShowOne
from cliff.lister import Lister
from .commons import DhusCommand

COLUMNS = ['Name', 'Description']


def set_option_parser(parser):
    parser.add_argument('--description', type=str, help='collection description')
    parser.add_argument('name', type=str, help='collection name')


def args_to_json(parsed_args):
    data = {'Name': parsed_args.name}
    try:
        data['Description'] = parsed_args.description
    except AttributeError:
        pass
    return data


def json_to_tuple(data):
    return data['Name'], data['Description']


def list_commands():
    return [
        DhusCommand(Create.NAME, Create.DESCRIPTION, Create),
        DhusCommand(Delete.NAME, Delete.DESCRIPTION, Delete),
        DhusCommand(List.NAME, List.DESCRIPTION, List),
        DhusCommand(Show.NAME, Show.DESCRIPTION, Show),
        DhusCommand(Update.NAME, Update.DESCRIPTION, Update)
    ]


class Create(ShowOne):
    NAME = 'collection create'
    DESCRIPTION = 'Create a collection'

    def __init__(self, app, app_args):
        super(Create, self).__init__(app, app_args, self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def get_parser(self, prog_name):
        parser = super(Create, self).get_parser(prog_name)
        set_option_parser(parser)
        return parser

    def take_action(self, parsed_args):
        data = args_to_json(parsed_args)
        json_data = json.JSONDecoder().decode(self.app.client.create('v2/Collections', data))
        return COLUMNS, json_to_tuple(json_data)


class Show(ShowOne):
    NAME = 'collection show'
    DESCRIPTION = 'Create a collection'

    def __init__(self, app, app_args):
        super(Show, self).__init__(app, app_args, self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def get_parser(self, prog_name):
        parser = super(Show, self).get_parser(prog_name)
        parser.add_argument('name', type=str, help='collection name')
        return parser

    def take_action(self, parsed_args):
        response = self.app.client.get("v2/Collections('" + parsed_args.name + "')")
        value = json_to_tuple(json.JSONDecoder().decode(response))
        return COLUMNS, value


class List(Lister):
    NAME = 'collection list'
    DESCRIPTION = 'List collection'

    def __init__(self, app, app_arg):
        super(List, self).__init__(app, app_arg, self.NAME)
        self.parser = self.get_parser(self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def take_action(self, parsed_args):
        values = []
        data = json.JSONDecoder().decode(self.app.client.get('v2/Collections'))
        for entry in data['value']:
            values.append(json_to_tuple(entry))
        return COLUMNS, values


class Update(Command):
    NAME = 'collection set'
    DESCRIPTION = 'Update a collection'

    def __init__(self, app, app_args):
        super(Update, self).__init__(app, app_args, self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def get_parser(self, prog_name):
        parser = super(Update, self).get_parser(prog_name)
        set_option_parser(parser)
        return parser

    def take_action(self, parsed_args):
        data = args_to_json(parsed_args)
        del data['Name']
        if self.app.client.update("v2/Collections('" + parsed_args + "')", data):
            return 0
        return 1


class Delete(Command):
    NAME = 'collection delete'
    DESCRIPTION = 'Delete collections'

    def __init__(self, app, app_args):
        super(Delete, self).__init__(app, app_args, self.NAME)

    def get_description(self):
        return self.DESCRIPTION

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(prog=prog_name)
        parser.add_argument('name', type=str, default=[], nargs='+', help='collection name')
        return parser

    def take_action(self, parsed_args):
        ret_value = 0
        for name in parsed_args.name:
            if not self.app.client.delete("v2/Collections('" + name + "')"):
                ret_value = 1
                self.app.sdterr.write('Cannot delete collection:' + name + '\n')
        return ret_value
