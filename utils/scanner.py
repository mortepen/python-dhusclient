import json

from cliff.command import Command
from cliff.show import ShowOne
from cliff.lister import Lister

from .commons import DhusCommand

DISPLAYABLE_LIST = ['Id', 'URL', 'Status']
DISPLAYABLE_READ = ['Id', 'URL', 'Status', 'Pattern', 'Scheduled']


def to_property_list(json_data):
    return [
        json_data['Id'],
        json_data['Url'],
        json_data['Status']
    ]


def to_property_read(json_data):
    return [
        json_data['Id'],
        json_data['Url'],
        json_data['Status'],
        json_data['Pattern'],
        json_data['Active']
    ]


class List(Lister):
    NAME = 'scanner list'
    DESCRIPTION = 'List scanners'

    def __init__(self, app, app_args):
        super(List, self).__init__(app, app_args, List.NAME)

    def get_description(self):
        return List.DESCRIPTION

    def take_action(self, parsed_args):
        rsp = self.app.client.get('v1/Scanners')
        data = json.JSONDecoder().decode(rsp)
        return DISPLAYABLE_LIST, to_property_list(data)


class Read(ShowOne):
    NAME = 'scanner show'
    DESCRIPTION = 'Show in detail a scanner'

    def __init__(self, app, app_args):
        super(Read, self).__init__(app, app_args, Read.NAME)

    def get_description(self):
        return List.DESCRIPTION

    def get_parser(self, prog_name):
        parser = super(Read, self).get_parser(prog_name)
        parser.add_argument('id', type=int, help='scanner id')
        return parser

    def take_action(self, parsed_args):
        rsp = self.app.client.get('v1/Scanners(' + parsed_args.id + ')')
        data = json.JSONDecoder().decode(rsp)
        return DISPLAYABLE_READ, to_property_read(data)


class Create(ShowOne):
    NAME = 'scanner create'
    DESCRIPTION = 'Create a scanner'
    pass


class Delete(Command):
    NAME = 'scanner delete'
    DESCRIPTION = 'Delete scanners'
    pass


def list_commands():
    return [
        DhusCommand(List.NAME, List.DESCRIPTION, List),
        DhusCommand(Read.NAME, Read.DESCRIPTION, Read)
    ]
