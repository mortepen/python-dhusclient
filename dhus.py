import argparse
import os
import sys

from cliff.app import App
from cliff.commandmanager import CommandManager
from cliff.help import HelpCommand

from utils import collection


class DhusCommandManager(CommandManager):
    """
      Allows to load and manage registered commands
    """

    def __init__(self):
        super(DhusCommandManager, self).__init__(None, True)
        self._cmd = []

    def load_commands(self, namespace):
        # collection commands
        for command in collection.list_commands():
            self._cmd.append(command)
            self.add_command(command.get_name(), command.get_class())

    def list_commands(self):
        return sorted(self._cmd, key=lambda cmd: cmd.get_name())


class _DhusHelpCommand(HelpCommand):
    """
      Allows to display help of a command
    """

    def __init__(self, app, app_args):
        super(_DhusHelpCommand, self).__init__(app, app_args)

    def take_action(self, parsed_args):
        try:
            factory, name, args = self.app.command_manager.find_command(self.app_args)
            self.app.stdout.write('print help of command: %s' % name)
        except ValueError:
            matches = self.app.get_fuzzy_matches(self.app_args[0])
            for match in matches:
                if match != 'help':
                    self.app.stdout.write('%s\n' % match)


class _DhusClientApp(App):
    """
      Main base application
    """

    def __init__(self, description, version):
        super(_DhusClientApp, self).__init__(description, version, DhusCommandManager(), deferred_help=True)
        self.command_manager = DhusCommandManager()
        self.command_manager.add_command('help', _DhusHelpCommand)
        self.command_manager.load_commands(None)
        self._set_streams(None, sys.stdout, sys.stderr)
        self.options = None
        self.client = None

    def configure_logging(self):
        pass

    def build_option_parser(self, description, version, argparse_kwargs=None):
        parser = argparse.ArgumentParser(prog='dhus', description=description, add_help=False)
        parser.add_argument('-h', '--help', dest='deferred_help', action='store_true', help='show help and exit')
        parser.add_argument(
            '-v', '--version',
            action='version',
            version=version,
            help='show program version'
        )
        parser.add_argument(
            '--dhus-url',
            metavar='URL',
            type=str,
            default=os.environ.get('DHUS_URL', None),
            help='DHuS URL. (default $DHUS_URL)'
        )
        parser.add_argument(
            '--dhus-usr',
            metavar='USERNAME',
            type=str,
            default=os.environ.get('DHUS_USR', None),
            help='DHuS username (default $DHUS_USR)'
        )
        parser.add_argument(
            '--dhus-pwd',
            metavar='PASSWORD',
            type=str,
            default=os.environ.get('DHUS_PWD', None),
            help='DHuS password (default $DHUS_PWD)'
        )
        return parser

    def run(self, argv):
        self.options, remainder = self.parser.parse_known_args(argv)
        self.configure_logging()
        if self.options.deferred_help and remainder:
            self.options.deferred_help = False
            remainder.insert(0, "help")
        self.initialize_app(remainder)
        self.print_help_if_requested()
        return self.run_subcommand(remainder)

    def run_subcommand(self, argv):
        try:
            sub_cmd = self.command_manager.find_command(argv)
        except ValueError:
            main_cmd = argv[0]
            fuzzy_matches = self.get_fuzzy_matches(main_cmd)
            if fuzzy_matches:
                self.stdout.write("Command %s not found\n" % main_cmd)
                self.stdout.write("Did you mean one of these?\n")
                for match in fuzzy_matches:
                    self.stdout.write("  %s\n" % match)
            return 1
        factory, name, args = sub_cmd
        cmd = factory(self, args)
        cmd_parser = cmd.get_parser(' '.join([self.NAME, name]))
        return cmd.run(cmd_parser.parse_args(args))


DESCRIPTION = 'Manage your DHuS easily'
VERSION = '0.0.1'


def main(param=sys.argv[1:]):
    return _DhusClientApp(DESCRIPTION, VERSION).run(param)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
